//APARIÇÃO DOS ITENS (ANIMAÇÃO)
const target = document.querySelectorAll('[data-anime]');
const animationClass = 'animate';

function animeScroll() {
  const windowTop = window.pageYOffset + (window.innerHeight * 0.75);
  target.forEach(function(element) {
    if((windowTop) > element.offsetTop) {
      element.classList.add(animationClass);
    } else {
      element.classList.remove(animationClass);
    }
  })
}

animeScroll();//SEMPRE ATIVAR A FUNÇÃO

if(target.length) {//VERIFICAR SE ALGUMA TAG TÁ UTILIZANDO O [data-anime]
  window.addEventListener('scroll', function() {
    animeScroll();
  }, 200);
}

//REFORÇO COM UM ALERTA (HEURÍSTICA)
function Alerta() {
    alert("Seu email foi enviado com sucesso");
}

//MOSTRAR OS POP UP DOS TRABALHOS
function iniciaModal(modalID){
  const modal = document.getElementById(modalID);
  modal.classList.add('mostrar');
  modal.addEventListener('click', (e)=> {
    if(e.target.id == modalID || e.target.className == 'fechar'){
      modal.classList.remove('mostrar');
    }
  });
}
